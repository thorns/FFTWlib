#! /bin/sh
# /*@@
#   @file      FFTW.sh
#   @date      Sun 11 September 2005
#   @author    Erik Schnetter
#   @desc
#              Setup for compilation with the FFTW library
#   @enddesc
# @@*/

# /*@@
#   @routine    CCTK_Search
#   @date       Wed Jul 21 11:16:35 1999
#   @author     Tom Goodale
#   @desc
#   Used to search for something in various directories
#   @enddesc
#@@*/

CCTK_Search()
{
  eval  $1=""
  if test $# -lt 4 ; then
    cctk_basedir=""
  else
    cctk_basedir="$4/"
  fi
  for cctk_place in $2
    do
      if test -r "$cctk_basedir$cctk_place/$3" ; then
        eval $1="$cctk_place"
        break
      fi
      if test -d "$cctk_basedir$cctk_place/$3" ; then
        eval $1="$cctk_place"
        break
      fi
    done
  return
}

# Search for FFTW installation
if test -z "$FFTW_DIR"; then
  echo "BEGIN MESSAGE"
  echo 'FFTW selected but no FFTW_DIR set... Checking some places'
  echo "END MESSAGE"
  CCTK_Search FFTW_DIR '/usr/lib /usr/local/lib' libfftw.a
  if test -z "$FFTW_DIR"; then
    CCTK_Search FFTW_DIR '/usr/lib /usr/local/lib' libfftw.so
  fi
  if test -z "$FFTW_DIR"; then
    echo "BEGIN ERROR"
    echo 'Unable to locate the FFTW library - please set FFTW_DIR'
    echo "END ERROR"
    exit 2
  fi
  echo "BEGIN MESSAGE" 
  echo "Found a FFTW package in $FFTW_DIR"
  echo "END MESSAGE"

elif test "$FFTW_DIR" = 'none'; then
  # user doesn't want the library path added
  FFTW_DIR=''
fi

if test -z "$FFTW_LIBS"; then
  if test -n "$MPI" -a "$MPI" != 'none' ; then
    FFTW_LIBS='rfftw_mpi fftw_mpi $(MPI_LIBS) rfftw fftw m'
  else
    FFTW_LIBS='rfftw fftw m'
  fi
fi

# Set the FFTW libs, libdirs and includedirs

# Don't explicitely add standard include and library search paths
if [ "$FFTW_DIR" != '/usr' -a "$FFTW_DIR" != '/usr/local' ]; then
  FFTW_INC_DIRS="$FFTW_DIR/include"
  FFTW_LIB_DIRS="$FFTW_DIR/lib"
fi

# Write the data out to the headers and makefiles
echo 'BEGIN MAKE_DEFINITION'
echo "FFTW_INC_DIRS = $FFTW_INC_DIRS"
echo "FFTW_LIB_DIRS = $FFTW_LIB_DIRS"
echo "FFTW_LIBS     = $FFTW_LIBS"
echo 'END MAKE_DEFINITION'

echo 'INCLUDE_DIRECTORY $(FFTW_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(FFTW_LIB_DIRS)'
echo 'LIBRARY $(FFTW_LIBS)'
